import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './modules/product/product.entity';
import { Order } from './modules/order/order.entity';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'motherlode',
    database: 'dbxdd',
    entities: [Product, Order],
    synchronize: true,
  }), TypeOrmModule.forFeature([Product, Order])],
})
export class DatabaseModule {}