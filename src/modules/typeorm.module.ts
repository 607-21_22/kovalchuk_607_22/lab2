import { Module } from '@nestjs/common'
import { TypeOrmModule as NestTypeOrmModule } from '@nestjs/typeorm'


@Module({
    imports: [
        NestTypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5432,
            username: 'admin',
            password: 'motherlode',
            database: 'storage_app',
            synchronize: true,
            autoLoadEntities: true,
            entities: ['dist/entities//*.entity.js'],
        })
    ]
})
export class TypeOrmModule { }